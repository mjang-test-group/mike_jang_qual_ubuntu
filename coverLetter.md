January 27, 2023

Hello,

I would like to join your team as a Senior Technical Author. I've used Ubuntu for well over a decade, and have written published books and created video courses on Ubuntu Server.

After chatting with Daniele Procida, I'm convinced that I can help. 

With my experience starting a documentation practice at Cobalt, I've learned to evangelize the role of documentation as a professional discipline.

I've created and maintained tutorials, how-to guides, references guides, and other explanitory material.

With my experience working with open source, for my books, and at GitLab, I know how to work with open soruce developers.

With my experience as a systems administrator, my RHCE knowledge, and my knowledge of code, I understand the documentation needs of DevOps users. I talked about this experience at the Open Source Convention [Link to 5 minute YouTube](https://www.youtube.com/watch?v=C0dwvMtd9KU)

As a writer, I test products myself, from the command-line, over REST, from the UI, and within code. I test changes to configuration files along with variations on REST calls. That practice earns respect among developers, and helps me write with authority.

I’ve worked in a “docs-as-code” setup since 2012. I've created GitHub repositories from scratch, with Hugo as our static site generator. I've shared this letter as a file in a dedicated GitLab repository.

I've shared my documentation expertise at conferences. For more information, see my [speaking experience](https://gitlab.com/mikejang/mike_jang_qual/-/blob/master/speakingExperience.md), including:

- API documentation at the [API Strategy Conference 2018](https://apistrat18.sched.com/speaker/michael.jang), updated for [API The Docs 2020](https://www.youtube.com/watch?v=E3YesNMUx1o)
- The command line at [Write the Docs](http://www.writethedocs.org/videos/portland/2019/how-i-learned-to-stop-worrying-and-love-the-command-line-mike-jang/) North America 2019
- UI Text at the [Open Source Convention (OSCON) 2017](https://youtu.be/qc3v5D1HHac)

I thank you for your consideration, and look forward to hearing from you!

Sincerely,  
Mike Jang  
michael@linuxexam.com  
(503) 381-7368

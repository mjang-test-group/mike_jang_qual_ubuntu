# Mike Jang's Qualifications

Mike Jang's qualifications for the <fill in blank> position.

- Resume: mjangResume.md
- Writing Samples: writingSamples.md
- Cover Letter: coverLetter.md
- Speaking Experience: speakingExperience.md
- LinkedIn Profile: https://www.linkedin.com/in/mijang/
